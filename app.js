Reveal.addEventListener('ready', function () {
    Services.buildSideNav();
});

Reveal.addEventListener('slidechanged', function (event) {
    Services.setActive();
});