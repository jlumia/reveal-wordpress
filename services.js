function Services() {
    this.section = $("section");
    this.buildSideNav = function () {

        //create the html string
        var htmlString = "";

        //compile the string
        $.each(this.section, function (i, v) {
            htmlString += "<div class='menu-item'>" + $(v).attr("nav-title") + "</div>";
        });

        //append it to the menu
        $(".nav-menu").append(htmlString);

        this.setActive();

        $(".menu-item").click(function () {
            Reveal.slide($(this).index());
        });

    };

    this.setActive = function () {
        $(".menu-item").removeClass("active");
        $($(".menu-item")[Reveal.getIndices().h]).addClass("active");
    };
}

var Services = new Services();